#  Audio Emotion Prediction Module


## Overview

* Extracts and visualize audio features from the Echonest.
* Extracts and visualize emotional responses from various DBs
* Train machine learning models for emotion prediction from EN descriptors

####Links and Email
<sylvain.legroux@fulbrightmail.org>

