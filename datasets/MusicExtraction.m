clear all;
close all;
% a = miraudio('vivaldi');
% w = miremotion(a,'Dimensions',3);
% t = get(w,'DimData');
% d = get(w,'ClassData');
% c =cell2mat(t{1,1});
% 

mDimdataSum = zeros(150,3);
for i = 23:25
    music = miraudio(['set1/mp3/Soundtrack360_mp3/0',num2str(i),'.mp3']);
    mu_emotion = miremotion(music,'Dimensions');
    Dimdata = get(mu_emotion,'DimData');
    mDimdata = cell2mat(Dimdata{1,1});
    mDimdataT = mDimdata';
    mDimdataSum(i,:) = mDimdataT;
end
% xlswrite('tempdata.xls',mDimdataSum);
