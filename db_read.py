#!/usr/bin/env python
from pylab import *

filename='datasets/set2/mean_ratings_set2.csv'
data = recfromcsv(filename, delimiter=',')
keys = data.dtype.fields.keys()

def main():
	figure()
	hist_emo3d()
	figure()
	bar_3D(1,data)
	figure()
	hist_emo5()
	figure()
	bar_5(1,data)

def hist_emo3d():
	suptitle("Distribution of 3D emotional ratings")
	subplot(3,1,1)
	hist(data['valence'])
	title('Valence')
	subplot(3,1,2)
	hist(data['arousal'])
	title('Arousal')
	subplot(3,1,3)
	hist(data['tension'])
	title('Tension')
	show()

def hist_emo5():
	suptitle("Distribution of discrete emotional ratings")
	subplot(3,2,1)
	hist(data['anger'])
	title('Anger')
	subplot(3,2,2)
	hist(data['fear'])
	title('Fear')
	subplot(3,2,3)
	hist(data['happy'])
	title('Happy')
	subplot(3,2,4)
	hist(data['sad'])
	title('Sad')
	subplot(3,2,5)
	hist(data['tender'])
	title('Tender')
	show()

def bar_3D(index,data):
	suptitle("Track 3D emotional ratings")
	pos = arange(3)+0.5
	data = data[index]
	bar(pos,[data['valence'],data['arousal'],data['tension']])
	xticks(pos,('Valence','arousal','tension'))
	show()

def bar_5(index,data):
	suptitle("Track discrete emotional rating")
	data=data[index]
	pos=arange(5)+0.5
	bar(pos,[data['anger'],data['fear'],data['happy'],data['sad'],data['tender']])
	xticks(pos,('Anger','Fear','Happy','Sad','Tender'))
	show()

if __name__=='__main__':
	main()