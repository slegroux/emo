emo_ratings Module
==================

.. automodule:: emo_ratings
    :members: hist_emo3d, hist_emo5, bar_3D, bar_5, parse_params
    :undoc-members:
    :show-inheritance:
