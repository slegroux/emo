.. Museaic Emotion documentation master file, created by
   sphinx-quickstart on Fri May 31 10:55:07 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Museaic Emotion's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 3

   en_features
   emo_ratings
   regression

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

