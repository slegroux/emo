#!/usr/bin/env python
# (c) 2013 Sylvain Le Groux

# Uncomment to set the API key explicitly. Otherwise Pyechonest will
# look in the ECHO_NEST_API_KEY environment variable for the key.
from pyechonest import config
config.ECHO_NEST_API_KEY = 'EDFJ6D8B4JF2PWXGR'
from pyechonest import song, track
from pylab import *
#import pdb
from IPython import embed
from optparse import OptionParser
from pandas import *
import os
import time
#import webbrowser
#import urllib2


def main():
	my_artist, my_title, my_soundfile, directory, output, plot, my_csv_list, args = parse_params()
	if my_soundfile:
		features = feature_from_file(my_soundfile)
		#embed()
		#dict2frame(features)
	elif (my_artist and my_title):
		features = feature_from_tag(my_artist, my_title)
		#dict2frame(features)
	elif directory:
		features = feature_from_dir(directory)
		#dict2frame(features)
	elif my_csv_list:
		#features = feature_from_spotify_list(my_csv_list)
		features = feature_from_list(my_csv_list)

	elif test:
		test("Pitchfork2012/")
	else:
		if (not(test)):
			print "error: requires artist AND title or soundfile for analyis"

	if output:
		dict2frame(features).to_csv(output)

	if plot and not(directory or my_csv_list):
		plot_features(features)

	elif plot and (directory or my_csv_list):
		print "plot and (directory or my_csv_list)"
		#embed()
		df = pandas.DataFrame(features)
		df.ix[:, 2:12].plot(logy=True, title="EN Features", use_index=True)
		figure()
		df.ix[:, 2:12].boxplot()
		show()
	else:
		print "No plotting option was chosen"


def dict2frame(my_dic):
	'''
		convert dictionary data structure into data frame (R-like)

		:param my_dic: a dictionary of data
		:returns: a pandas data frame
	'''
	if size(my_dic) > 1:
		frame = pandas.DataFrame(my_dic)
	else:
		frame = pandas.DataFrame.from_dict(my_dic, orient='index').T
	return frame


def test(my_dir):
	'''test function'''
	print "-----------inside test----------"
	'''artist=artist.Artist(options.artist)
	file_image=urllib2.urlopen(artist.images[0]['url'])
	webbrowser.open(artist.images[0]['url'])
	'''
	try:
		print "try the test"
	except:
		print '--------------No songs found-------------'


def feature_from_dir(my_dir):
	'''
		Extract features from directory of local tracks

		:param my_dir: directory of sound files
		:returns: a pandas data frame of features
	'''
	start = time.time()
	feat = []
	print "------------starting directory analysis-----------"
	for f in os.listdir(my_dir):
		path = os.path.join(my_dir, f)
		#embed()
		if os.path.splitext(f)[1].lower().endswith(('.mp3', '.wav', '.ogg', '.m4a', '.mp4')):
			track_feat = feature_from_file(path)
			time.sleep(4)  # due to echonest limitations ask for special license if more speed is needed
			feat.append(track_feat)
		else:
			print f, "is not a recognized sound file format"

	end = time.time()
	print "-------------Directory analysis time: %f s.-----------------" % (end-start)
	return feat


def feature_from_file(soundfile):
	'''
		Extract feature from local soundfile

		:param soundfile: sound file
		:returns: a pandas data frame of features
	'''
	#fileName, fileExtension = os.path.splitext(soundfile)
	#f = open(soundfile)
	#t = track.track_from_file(f, fileExtension)

	t = track.track_from_filename(soundfile, timeout=100000)  # timeout to avoid socket.timeout: timed out error
	#t = track.track_from_filename(soundfile)
	start = time.time()
	print "------------starting track analysis-----------"
	#embed()
	try:
		feat = {'artist': t.artist, 'title': t.title, 'analysis_url': t.analysis_url, 'danceability': t.danceability, 'duration': t.duration, 'energy': t.energy, 'key': t.key, 'liveness': t.liveness, 'loudness': t.loudness, 'mode': t.mode, 'speechiness': t.speechiness, 'tempo': t.tempo, 'time_signature': t.time_signature}
	except AttributeError:
		feat = {'artist': "Unknow", 'title': soundfile, 'analysis_url': t.analysis_url, 'danceability': t.danceability, 'duration': t.duration, 'energy': t.energy, 'key': t.key, 'liveness': t.liveness, 'loudness': t.loudness, 'mode': t.mode, 'speechiness': t.speechiness, 'tempo': t.tempo, 'time_signature': t.time_signature}
	end = time.time()
	print "-------------Track analysis time: %f s.--------------" % (end-start)
	return feat


def feature_from_tag(artist, title):
	'''
		Extract feature from artist name and track title in Echonest DB

		:param artist: name of artist
		:type artist: string
		:param title: title of song
		:type title: string
		:returns: features
		:rtype: pandas data frame
	'''
	try:
		song_list = song.search(artist=artist, title=title)
		my_song = song_list[0]
		print "------------Found " + title + " by " + artist + " on the Echonest-----"
		start = time.time()
		print "------------starting track analysis-----------"
		feat = my_song.audio_summary
		feat['artist'] = my_song.artist_name
		feat['title'] = my_song.title
		end = time.time()
		print "-------------Track analysis time: %f s.--------------" % (end-start)
		return feat
	except:
		print '--------------No songs found---- for: ', artist, song
	# print 'Audio summary of "%s" by %s:' % (ts.title,ts.artist_name)
	#for aud_key, aud_val in ts.audio_summary.iteritems():
	#   print '    %-15s %s' % (aud_key, aud_val)


def feature_from_spotify_id(id):
	'''
		Extract feature from songs with spotify IDs

		:param artist: name of artist
		:type artist: string
		:param title: title of song
		:type title: string
		:returns: features
		:rtype: pandas data frame
	'''
	try:
		
		my_song = song.Song(id, buckets=['id:spotify-WW'])
		print "------------Found your song!-----------"
		start = time.time()
		print "------------starting track analysis-----------"
		feat = my_song.audio_summary
		feat['artist'] = my_song.artist_name
		feat['title'] = my_song.title
		end = time.time()
		print "-------------Track analysis time: %f s.--------------" % (end-start)
		return feat
	except:
		print '--------------No songs found---- '
	# print 'Audio summary of "%s" by %s:' % (ts.title,ts.artist_name)
	#for aud_key, aud_val in ts.audio_summary.iteritems():
	#   print '    %-15s %s' % (aud_key, aud_val)

def feature_from_spotify_list(my_csv_list):
	filename = my_csv_list
	data = recfromcsv(filename, delimiter=',')
	keys = data.dtype.fields.keys()
	start = time.time()
	feat = []
	print "------------starting directory analysis-----------"
	for i in range(len(data)):
		track_feat = feature_from_spotify_id(data['spotify_track_id'])
		time.sleep(5)  # due to echonest limitations ask for special license if more speed is needed
		feat.append(track_feat)
	end = time.time()
	print "-------------Directory analysis time: %f s.-----------------" % (end-start)
	return feat


def feature_from_list(my_csv_list):
	# !!!! bug somewhere later in pandas conversions
	#filename='datasets/set2/mean_ratings_set2.csv'
	filename = my_csv_list
	data = recfromcsv(filename, delimiter=',')
	keys = data.dtype.fields.keys()
	start = time.time()
	feat = []
	print "------------starting directory analysis-----------"
	for i in range(len(data)):
		track_feat = feature_from_tag(data['artist'][i], data['song'][i])
		time.sleep(4)  # due to echonest limitations ask for special license if more speed is needed
		feat.append(track_feat)
	end = time.time()
	print "-------------Directory analysis time: %f s.-----------------" % (end-start)
	return feat


def parse_params():
	''' Parse input parameters to command line tool'''
	#TO DO: make a class with a method to retrieve error messages
	parser = OptionParser(usage="%prog [-p] [-a] [-t] [-f] [-d] [-l] [-o] [-T]", version="%prog 1.0")
	parser.add_option("-a", "--artist", dest="artist", help="Artist name of the track to analyze")
	parser.add_option("-t", "--title", dest="title", help="Title of the track to analyze")
	parser.add_option("-f", "--file", dest="file", help="Audio file to analyze")
	parser.add_option("-d", "--dir", dest="directory", help="Directory of audio files to analyze")
	parser.add_option("-o", "--output", dest="output", help="Write feature analysis to csv file")
	parser.add_option("-p", "--plot", action="store_true", default=False, dest="plot", help="Plot analysis features")
	parser.add_option("-l", "--list", dest="csvlist", help="List of songs in a csv file")
	parser.add_option("-T", "--test", action="store_true", default=False, dest="test", help="Run the test module")

	#parser.add_option("-e", "--example", dest="example", action="store_true", default=False, help="test/example")
	(options, args) = parser.parse_args()
	return options.artist, options.title, options.file, options.directory, options.output, options.plot, options.csvlist, args


def plot_features(features):
	'''
		Visualize features analyzed for single track (barplot) or whole directory (boxplot)

		:param features: feature analysis data
		:type features: pandas data frame
	'''
	key = ("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
	mode = ("major", "minor")
	f = figure(1)
	#f.text(.5,.95,ts.title+' by '+ts.artist_name,horizontalalignment='center')
	#embed()
	suptitle(features['title']+' by '+features['artist'], fontsize=16, fontweight='bold')

	subplot(2, 2, 1)
	pos = arange(4)+0.5
	#pdb.set_trace()
	#barh(pos,[audio['danceability'],audio['duration'],audio['energy'],audio['key'],audio['liveness'],audio['loudness'],audio['mode'],audio['speechiness'],audio['tempo'],audio['time_signature']])
	barh(pos,[features['danceability'],features['energy'],features['liveness'],features['speechiness']],align='center')
	#yticks(pos,('dance','duration','energy','key','liveness','loudness','mode','speechiness','tempo','time_signature'))
	yticks(pos,('dance','energy','liveness','speech'))
	#xlabel(ts.title+' by '+ts.artist_name)
	title('Echonest descriptors', fontweight='bold')
	axis([0.0, 1.0, 0, 4])

	subplot(2, 2, 2)
	title('Loudness (dB)', fontweight='bold')
	bar([1],features['loudness'],width=1)
	axis([0,3,-20,0])

	subplot(2,2,3)
	title('Tempo (BPM)', fontweight='bold')
	bar([1],features['tempo'],width=1)
	axis([0,3,30,200])

	subplot(2,2,4)
	title('Other properties', fontweight='bold')
	axis([0.0,1.0,0,4])
	xpos=0.2
	ypos=3.0
	text(xpos,ypos,'Duration: '+str(features['duration'])+' s')
	#text(xpos,ypos-0.25,'Tempo: '+str(features['tempo'])+' BPM')
	#text(xpos,ypos-0.5,'Loudness: '+str(features['loudness'])+' dB')
	text(xpos,ypos-0.5,'Key: '+key[features['key']])
	text(xpos,ypos-1,'Mode: '+mode[features['mode']])
	text(xpos,ypos-1.5,'Signature: '+str(features['time_signature']))
	show()

if __name__ == '__main__':
	main()
