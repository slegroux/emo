#!/usr/bin/env python
# (c) 2013 Sylvain Le Groux

#from pandas import *
#from pylab import *
from regression import *

input_csv = 'datasets/set2/set2.csv'
output_csv = 'datasets/set2/mean_ratings_set2.csv'
scales, discrete, esthetics, labels, features, big_df = read_dbs(input_csv, output_csv)
data = export2arff(big_df)
print arff.dumps(data)
