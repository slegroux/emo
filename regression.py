#!/usr/bin/env python
# (c) 2013 Sylvain Le Groux

from pandas import *
from pylab import *
import arff
from optparse import OptionParser
from sklearn import svm
from sklearn import preprocessing
from sklearn import cross_validation
from sklearn import grid_search
from sklearn.metrics import explained_variance_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from IPython import embed
from sklearn.decomposition import PCA


def main():
	input_csv, output_csv, plot, verbose, args = parse_params()
	if not(input_csv and output_csv):
		print "--- using defaults data sets for features: set2/set2.csv and ratings: mean_ratings_set2.csv ---"
		input_csv = 'datasets/set2/set2.csv'
		output_csv = 'datasets/set2/mean_ratings_set2.csv'

	# read data from csv-formatted db output data frames
	scales, discrete, esthetics, labels, features, big_df = read_dbs(input_csv, output_csv)
	# input/features
	X = features.values
	# output/target
	y = scales.values
	# scaling
	X = preprocess(X)
	# test algorithm on subset of data to gain learning time
	#X,y=subset(X,y,100)

	# split dataset into training/testing set. X features, Y targets
	X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.3, random_state=0)
	# train 3 models, one for each emotional rating with grid search
	clf_energy, clf_valence, clf_tension = train_EVT(X_train, y_train)

	'''valence_train = y_train[:, 0]
	valence_test = y_test[:, 0]
	energy_train = y_train[:, 1]
	energy_test = y_test[:, 1]
	tension_train = y_train[:, 2]
	tension_test = y_test[:, 2]'''

	print "--------- Training set-----------------"
	valence_train_prediction = clf_valence.predict(X_train)
	evaluate(y_train[:, 0], valence_train_prediction)

	print "--------- Test set---------------------"
	valence_test_prediction = clf_valence.predict(X_test)
	evaluate(y_test[:, 0], valence_test_prediction)

	print "--------- Plot    ---------------------"
	if (plot):
		plot_evaluation(y_train[:, 0], valence_train_prediction)
		plot_evaluation(y_train[:, 0], valence_test_prediction)

	'''print "--------- Training set-----------------"
	y_true, y_pred = energy_train, clf_energy.predict(X_train)
	evaluate(y_true, y_pred)
	print "--------- Test set---------------------"
	y_true, y_pred = energy_test, clf_energy.predict(X_test)
	evaluate(y_true, y_pred)

	print "--------- Training set-----------------"
	y_true, y_pred = tension_train, clf_tension.predict(X_train)
	evaluate(y_true, y_pred)
	print "--------- Test set---------------------"
	y_true, y_pred = energy_test, clf_tension.predict(X_test)
	evaluate(y_true, y_pred)'''

	'''clf_energy = svm.SVR()
	print clf_energy.fit(X_train, y_train[:, 1])
	print clf_energy.score(X_test, y_test[:, 1])
	clf_tension = svm.SVR()
	print clf_tension.fit(X_train, y_train[:, 2])
	print clf_tension.score(X_test, y_test[:, 2])'''


def train_EVT(X_train, y_train):
	# each target is trained separately (due to libsvm)
	valence_train = y_train[:, 0]
	#valence_test = y_test[:, 0]
	energy_train = y_train[:, 1]
	#energy_test = y_test[:, 1]
	tension_train = y_train[:, 2]
	#tension_test = y_test[:, 2]
	# valence
	clf_valence = train_grid(X_train, valence_train)
	# energy
	clf_energy = train_grid(X_train, energy_train)
	# tension
	clf_tension = train_grid(X_train, tension_train)
	return clf_energy, clf_valence, clf_tension


def evaluate(y_true, y_pred):
	#print "--------- Scores: \n", clf_valence.score(X_test, valence_test)
	print "--------- Mean squared error: ", mean_squared_error(y_true, y_pred)
	#print "--------- Real valence\n %s\n -------prediction:\n %s" % (y_true, y_pred)
	print "--------- Explained variance: ", explained_variance_score(y_true, y_pred)
	print "--------- R coefficient: ", r2_score(y_true, y_pred)


def plot_evaluation(y_true,y_pred):
		plot(y_true, 'kx'), plot(y_pred, 'r--')
		show()


def train_grid(X_train, valence_train):
	# Grid search on range of hyperparameters
	C = max(valence_train) - min(valence_train)
	#tuned_parameters = {'kernel': ['rbf', 'linear'], 'epsilon': [0.01, 0.1, 0.5], 'gamma': [1e-3, 1e-4, 1e-5], 'C': [1, 10, 50, 60, 100, 110, 1000]}
	tuned_parameters = {'epsilon': arange(0, 5), 'gamma': arange(2**-7, 2**7, 2), 'C': [C]}
	svr = svm.SVR()
	clf_valence = grid_search.GridSearchCV(svr, tuned_parameters)
	#embed()
	print "--------- Start learning SVR ----------"
	clf_valence.fit(X_train, valence_train, cv=5)
	print "--------- Learning done ---------------"
	print "--------- Best estimator: \n", clf_valence.best_estimator_
	#print "--------- Grid scores: \n", clf_valence.grid_scores_
	return(clf_valence)


def subset(X, y, n_samples):
	X = X[0:n_samples, :]
	y = y[0:n_samples, :]
	return X, y


def preprocess(X):
	# normalize data
	'''scaler = preprocessing.StandardScaler()
	X_scaled = scaler.fit_transform(X)
	X = X_scaled'''
	#embed()
	pca = PCA(n_components=3, whiten=True).fit(X)
	X_pca = pca.transform(X)
	X_scaled = X_pca
	#min_max_scaler = preprocessing.MinMaxScaler((0, 1))
	#X_scaled = min_max_scaler.fit_transform(X)
	#X = X_scaled
	'''normalizer = preprocessing.Normalizer()
	X_normalized = normalizer.fit_transform(X)
	X = X_normalized'''
	#test = min_max_scaler.inverse_transform(X_mm)
	return X_scaled


def read_dbs(feature_filename, target_filename):
	'''
		read csv files output data frames

		:param feature_filename: audio features values as csv file
		:type feature_filename: string
		:param target_filename: cvs file with emotional target values
		:type target_filename: string
		:returns: scales [valence, energy, tension], discrete ['anger', 'fear', 'happy', 'sad', 'tender']
			, esthetics [beauty, liking] labels/targets and features ['danceability', 'energy', 'liveness', 'loudness', 'tempo']
		:rtype: pandas data frame
	'''
	# specific to set2 & EN analysis formats...
	#input_csv = 'datasets/set2/set2.csv'
	#en_features = pandas.read_csv('datasets/set2/set2.csv')
	#ratings = pandas.read_csv('datasets/set2/mean_ratings_set2.csv')
	#output_csv = 'datasets/set2/mean_ratings_set2.csv'
	# OUTPUT
	ratings = pandas.read_csv(target_filename)
	#scales = df.ix[:, 1:11]
	scales = ratings[['valence', 'arousal', 'tension']]
	discrete = ratings[['anger', 'fear', 'happy', 'sad', 'tender']]
	esthetics = ratings[['beauty', 'liking']]
	labels = ratings['TARGET']
	# INPUT
	en_features = pandas.read_csv(feature_filename)
	features = en_features[['danceability', 'duration', 'energy', 'key', 'liveness', 'loudness', 'mode', 'speechiness', 'tempo']]
	#features = en_features[['danceability', 'energy', 'liveness', 'loudness', 'tempo']]
	big_df = concat([en_features, ratings], axis=1)
	return scales, discrete, esthetics, labels, features, big_df


def export2arff(big_df):
	# specific to emo data set. easy to generalize later...
	data = {}
	data['data'] = big_df.values
	data['description'] = u'Audio features and emotional ratings'
	data['relation'] = 'Emotion110'
	data['attributes'] = [
		('Unnamed 0:', 'INTEGER'),
		('analysis_url', 'STRING'),
		('artist', 'STRING'),
		('danceability','REAL'),
		('duration', 'REAL'),
		('energy', 'REAL'),
		('key', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
		('liveness', 'REAL'),
		('loudness', 'REAL'),
		('mode', [0,1]),
		('speechiness', 'REAL'),
		('tempo', 'REAL'),
		('time_signature','INTEGER'),
		('title','STRING'),
		('Number','INTEGER'),
		('valence','REAL'),
		('arousal','REAL'),
		('tension','REAL'),
		('anger','REAL'),
		('fear','REAL'),
		('happy','REAL'),
		('sad','REAL'),
		('tender','REAL'),
		('beauty','REAL'),
		('liking','REAL'),
		('TARGET','STRING'),#['ANGER_HIGH', 'ANGER_MODERATE','FEAR_HIGH','FEAR_MODERATE', 'HAPPY_HIGH','HAPPY_MODERATE']),
		('Soundtrack','STRING'),
		('Index in Set 1','INTEGER')
	]
	return data


def parse_params():
	''' Parse input parameters to command line tool'''
	#TO DO: make a class with a method to retrieve error messages
	parser = OptionParser(usage="%prog [-p] [-a] [-t] [-f] [-d] [-o] [-T]", version="%prog 1.0")
	parser.add_option("-i", "--input", dest="input", help="csv file with input features")
	parser.add_option("-o", "--output", dest="output", help="csv file with targets")
	parser.add_option("-p", "--plot", action="store_true", default=False, dest="plot", help="Plot analysis features")
	parser.add_option("-V", "--verbose", action="store_true", default=False, dest="verbose", help="Output results to terminal")

	#parser.add_option("-e", "--example", dest="example", action="store_true", default=False, help="test/example")
	(options, args) = parser.parse_args()
	return options.input, options.output, options.plot, options.verbose, args


if __name__ == '__main__':
	main()
