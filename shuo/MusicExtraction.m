% clear all;
% close all;
% a = miraudio('vivaldi');
% w = miremotion(a,'Dimensions',3);
% t = get(w,'DimData');
% d = get(w,'ClassData');
% c =cell2mat(t{1,1});
% 

% mDimdataSum = zeros(150,4);
% for i = 27:27
%     music = miraudio(['set2/mp3/0',num2str(i),'.mp3']);
%     mu_emotion = miremotion(music,'Tension');
%     Dimdata = get(mu_emotion,'TensionFactors');
%     mDimdata = cell2mat(Dimdata{1,1});
%     mDimdataT = mDimdata';
%     mDimdataSum(i,:) = mDimdataT;
% end
% xlswrite('tempdata.xls',mDimdataSum);


%-------------calculate tension values------------%
% tensionValue = zeros(360,5);
% for i = 100:360
%     x = miraudio(['set1/mp3/',num2str(i),'.mp3']);
%     rm = mirrms(x,'Frame',.046,.5);
%     fl = mirfluctuation(x,'Summary');
%     fp = mirpeaks(fl,'Total',1);
%     c = mirchromagram(x,'Frame','Wrap',0,'Pitch',0);    %%%%%%%%%%%%%%%%%%%% Previous frame size was too small.
%     cp = mirpeaks(c,'Total',1);
%     ps = 0;%cp;
%     ks = mirkeystrength(c);
%     [k kc] = mirkey(ks);
%     mo = mirmode(ks);
%     hc = mirhcdf(c);
%     nr = mirnovelty(mirchromagram(x,'Frame',.2,.25,'Wrap',0),'Normal',0);
%     y = {rm, fp, kc, hc,nr};
%     z = get(y{1},'Data');
%     z = cell2mat(z{1,1});
%     RM = std(z);
%     z2 = get(y{2},'Data');
%     z2 = cell2mat(z2{1,1});
%     FPV = mean(z2);
%     z3 = get(y{3},'Data');
%     z3 = cell2mat(z3{1,1});
%     KC = mean(z3);
%     z4 = get(y{4},'Data');
%     z4 = cell2mat(z4{1,1});
%     HC = mean(z4);
%     z5 = get(y{5},'Data');
%     z5 = cell2mat(z5{1,1});
%     z5(isnan(z5)) = [];
%     NR = mean(z5);
%     output = [RM, FPV, KC, HC, NR];
%     tensionValue(i,:) = output;
% end





%-------------calculate valence values------------%
% valenceValue = zeros(360,5);
% for i = 100:360
%     x = miraudio(['set1/mp3/',num2str(i),'.mp3']);
%     rm = mirrms(x,'Frame',.046,.5);
%     fl = mirfluctuation(x,'Summary');
%     fp = mirpeaks(fl,'Total',1);
%     c = mirchromagram(x,'Frame','Wrap',0,'Pitch',0);    %%%%%%%%%%%%%%%%%%%% Previous frame size was too small.
%     cp = mirpeaks(c,'Total',1);
%     ps = 0;%cp;
%     ks = mirkeystrength(c);
%     [k kc] = mirkey(ks);
%     mo = mirmode(ks);
%     hc = mirhcdf(c);
%     ns = mirnovelty(mirspectrum(x,'Frame',.1,.5,'Max',5000),'Normal',0);
%     y = {rm, fp, kc, mo, ns};
%     z = get(y{1},'Data');
%     z = cell2mat(z{1,1});
%     RM = std(z);
%     z2 = get(y{2},'Data');
%     z2 = cell2mat(z2{1,1});
%     FPV = mean(z2);
%     z3 = get(y{3},'Data');
%     z3 = cell2mat(z3{1,1});
%     KC = mean(z3);
%     z4 = get(y{4},'Data');
%     z4 = cell2mat(z4{1,1});
%     MO = mean(z4);
%     z5 = get(y{5},'Data');
%     z5 = cell2mat(z5{1,1});
%     z5(isnan(z5)) = [];
%     NS = mean(z5);
%     output = [RM, FPV, KC, MO, NS];
%     valenceValue(i,:) = output;
% end


%-------------calculate energy values------------%
energyValue = zeros(360,4);
for i = 343:343
    x = miraudio(['set1/mp3/',num2str(i),'.mp3']);
    rm = mirrms(x,'Frame',.046,.5);
    fl = mirfluctuation(x,'Summary');
    fp = mirpeaks(fl,'Total',1);
    s = mirspectrum(x,'Frame',.046,.5);
    sc = mircentroid(s);
    ss = mirspread(s);
    sr = mirroughness(s);
%     se = mirentropy(mirspectrum(x,'Collapsed','Min',40,'Smooth',70,'Frame',1.5,.5)); %%%%%%%%% Why 'Frame'?? 
%     y = {rm, fp, sc, ss, se};
    y = {rm, fp, sc, ss};
    z = get(y{1},'Data');
    z = cell2mat(z{1,1});
    RM = mean(z);
    z2 = get(y{2},'Data');
    z2 = cell2mat(z2{1,1});
    FPV = mean(z2);
    z3 = get(y{3},'Data');
    z3 = z3{1,1};
    z3 = cell2mat(z3{1,1});
    z3(isnan(z3)) = [];
    SC = mean(z3);
    z4 = get(y{4},'Data');
    z4 = z4{1,1};
    z4 = cell2mat(z4{1,1});
    z4(isnan(z4)) = [];
    SS = mean(z4);
%     z5 = get(y{5},'Data');
%     z5 = cell2mat(z5{1,1});
%     z5(isnan(z5)) = [];
%     SE = mean(z5);
%     output = [RM, FPV, SC, SS, SE];
    output = [RM, FPV, SC, SS];
    energyValue(i,:) = output;
end