% %---multiple linear regression---%
% %--compare result---%
% tensionP = zeros(360,5);
% valenceP = zeros(360,5);
% energyP = zeros(360,5);

% %---calculate emotional values---%
% %---tension---%
% tensionValueR = zeros(360,1);
% for i = 1:360
%     tensionValueR(i,1) = 354.533 * tensionP(i,1)-0.002*tensionP(i,2)-8.962*tensionP(i,3)+14.194*tensionP(i,4)-0.012*tensionP(i,5)+11.058;
% end
% %---valence---%
% valenceValueR = zeros(360,1);
% for i = 1:360
%     valenceValueR(i,1) = -149.895*valenceP(i,1)+0.003*valenceP(i,2)+10.138*valenceP(i,3)+10.186*valenceP(i,4)+0.014*valenceP(i,5)-3.111;
% end
% %---energy---%
% energyValueR = zeros(360,1);
% for i = 1:360
%     energyValueR(i,1) = 363.097*energyP(i,1)+0.003*energyP(i,2)+0.001*energyP(i,3)-0.003*energyP(i,4)+31.27*energyP(i,5)-27.126;
% end
%---change scale to 1~7---%
% stv = zeros(360,1);
% svv = zeros(360,1);
% sev = zeros(360,1);
% for i = 1:360
%     stv(i) = tensionValueR(i)/max(tensionValueR)*7;
%     svv(i) = valenceValueR(i)/max(valenceValueR)*7;
%     sev(i) = energyValueR(i)/max(energyValueR)*7;
% end
%---calculate the error and std of error ---%
% set1tv = zeros(360,1);
% set1vv = zeros(360,1);
% set1ev = zeros(360,1);
% errortv = set1tv - stv;
% errorvv = set1vv - svv;
% errorev = set1ev - sev;
% terrormean = mean(errortv);
% verrormean = mean(errorvv);
% eerrormean = mean(errorev);
% stderrort = std(errortv);
% stderrorv = std(errorvv);
% stderrore = std(errorev);

%---use miremotion formula to calculate values---%
%---tension---%
% mirtensionValueR = zeros(360,1);
% for i = 1:360
%     mirtensionValueR(i,1) = 0.5382 * (tensionP(i,1)-0.024254)/0.015667-0.5406*(tensionP(i,2)-13270.1836)/10790.655-0.6808*(tensionP(i,3)-0.5124)/0.092+0.8629*(tensionP(i,4)-0.2962)/0.0459-0.5958*(tensionP(i,5)-71.8426)/46.9246+5.4679;
% end
% ---valence---%
% mirvalenceValueR = zeros(360,1);
% for i = 1:360
%     mirvalenceValueR(i,1) = -0.3161*(valenceP(i,1)-0.024254)/0.015667+0.6099*(valenceP(i,2)-13270.1836)/10790.655+0.8802*(valenceP(i,3)-0.5123)/0.091953+0.4565*(valenceP(i,4)+0.0019958)/0.048664+0.4015*(valenceP(i,5)-131.9503)/47.6463+5.2749;
% end
% ---energy---%
% mirenergyValueR = zeros(360,1);
% for i = 1:360
%     mirenergyValueR(i,1) = 0.6664*(energyP(i,1)-0.0559)/0.0337+0.6099*(energyP(i,2)-13270.1836)/10790.655+0.4486*(energyP(i,3)-1677.7)/570.34-0.4639*(energyP(i,4)-250.5574)/205.3147+0.7056*(energyP(i,5)-0.954)/0.0258+5.4861;
% end
%---change scale to 1~7---%
% mirstv = zeros(360,1);
% mirsvv = zeros(360,1);
% mirsev = zeros(360,1);
% for i = 1:360
%     mirstv(i) = mirtensionValueR(i)/max(abs(mirtensionValueR))*7;
%     mirsvv(i) = mirvalenceValueR(i)/max(mirvalenceValueR)*7;
%     mirsev(i) = mirenergyValueR(i)/max(mirenergyValueR)*7;
% end
%---calculate the error and std of error ---%
% mirerrortv = set1tv - mirstv;
% mirerrorvv = set1vv - mirsvv;
% mirerrorev = set1ev - mirsev;
% mirterrormean = mean(mirerrortv);
% mirverrormean = mean(mirerrorvv);
% mireerrormean = mean(mirerrorev);
% mirstderrort = std(mirerrortv);
% mirstderrorv = std(mirerrorvv);
% mirstderrore = std(mirerrorev);

% %---abs error $ calculate mean and std of abs error values---%
% absErrorEV = abs(errorev);
% absErrorTV = abs(errortv);
% absErrorVV = abs(errorvv);
% absTerrorMean = mean(absErrorTV);
% absVerrorMean = mean(absErrorVV);
% absEerrorMean = mean(absErrorEV);
% absStderrorT = std(absErrorTV);
% absStderrorV = std(absErrorVV);
% absStderrorE = std(absErrorEV);

% MirabsErrorEV = abs(mirerrorev);
% MirabsErrorTV = abs(mirerrortv);
% MirabsErrorVV = abs(mirerrorvv);
% MirabsTerrorMean = mean(MirabsErrorTV);
% MirabsVerrorMean = mean(MirabsErrorVV);
% MirabsEerrorMean = mean(MirabsErrorEV);
% MirabsStderrorT = std(MirabsErrorTV);
% MirabsStderrorV = std(MirabsErrorVV);
% MirabsStderrorE = std(MirabsErrorEV);
% myT_mse = mean(errortv.*errortv);
% myV_mse = mean(errorvv.*errorvv);
% myE_mse = mean(errorev.*errorev);




%%80regression 20 prediction
% clear all;
% orignal = zeros(88,1);
% current = zeros(88,1);
mse = mean((orignal-current).*(orignal-current));
%---tension---%
% parameter = zeros(22,20);
% prediction = zeros(22,1);
% value = zeros(22,1);
% +196.132*parameter(i,12)
for i = 1:22
    prediction(i,1)= 7.896*parameter(i,1)+36.056*parameter(i,2)+55.148*parameter(i,3)-2.766*parameter(i,4)+17.841*parameter(i,5)-0.5*parameter(i,6)-0.836*parameter(i,7)+0.884*parameter(i,8)-3.802*parameter(i,9)-5.991*parameter(i,10)+2.350*parameter(i,11)-17.984*parameter(i,12)-0.154*parameter(i,13)+1.460*parameter(i,14)+1.907*parameter(i,15)-5.996*parameter(i,16)-9.603*parameter(i,17)-28.739*parameter(i,18)-1.003*parameter(i,19)-3.827*parameter(i,20)-33.719;
end

% for i = 1:22
%     prediction(i,1)= 292.755*parameter(i,1)+0.006*parameter(i,2)+0.002*parameter(i,3)-0.007*parameter(i,4)-1.158*parameter(i,5)+3.003;%%+1.243*parameter(i,6)-1.809*parameter(i,7)-0.395*parameter(i,8)-56.612*parameter(i,9)-7.940*parameter(i,10)+5.994*parameter(i,11)+14.139*parameter(i,13)+50.345*parameter(i,14)-99.535*parameter(i,15)+1.475*parameter(i,16)-13.460*parameter(i,17)-7.615*parameter(i,18)+0.480*parameter(i,19)+75.444*parameter(i,20)+0.01;
% end
% 
Pmse = mean((value-prediction).*(value-prediction));