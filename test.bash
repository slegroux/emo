#!/bin/bash

#-------FEATURE EXTRACTION------
./en_features.py -h
# pre-analysis from EN DB
./en_features.py -p -a "James Blake" -t "retrograde"
# audio file is recognized & already in EN DB
./en_features.py -p -f "Pitchfork2012/05. Japandroids - The House That Heaven Built.mp3"
# new audio file is uploaded
./en_features.py -p -f "datasets/set2/mp3/002.mp3"
# analyze whole sound file directory and save as csv file
./en_features.py -p -d "sounds" -o "sounds.csv"
#./en_features.py -d "datasets/set2/mp3" -o "datasets/set2/set2.csv"
#./en_features.py -p -l "datasets/Julia_07112013.csv"
./en_features.py -l "datasets/Julia_light.csv"

#-------EMOTIONAL RATINGS--------
./emo_ratings.py -h
./emo_ratings.py -i 'datasets/set2/mean_ratings_set2.csv'

#--------REGRESSION--------------
./regression.py -h
./regression.py -i 'datasets/set2/set2.csv' -o 'datasets/set2/mean_ratings_set2.csv'